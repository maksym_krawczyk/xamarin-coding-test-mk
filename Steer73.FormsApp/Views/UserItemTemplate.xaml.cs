using Xamarin.Forms.Xaml;

namespace Steer73.FormsApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserItemTemplate
    {
        public UserItemTemplate()
        {
            InitializeComponent();
        }
    }
}