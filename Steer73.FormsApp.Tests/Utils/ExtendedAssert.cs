using System.Collections;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Steer73.FormsApp.Tests.Utils
{
    public static class ExtendedAssert
    {
        public static void EnumerableEqual(IEnumerable expected, IEnumerable actual)
        {
            var expectedJson = JsonConvert.SerializeObject(expected);
            var actualJson = JsonConvert.SerializeObject(actual);

            Assert.AreEqual(expectedJson, actualJson);
        }
    }
}