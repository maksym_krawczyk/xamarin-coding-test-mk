﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using Moq;
using NUnit.Framework;
using Steer73.FormsApp.Framework;
using Steer73.FormsApp.Model;
using Steer73.FormsApp.Tests.Utils;
using Steer73.FormsApp.ViewModels;

namespace Steer73.FormsApp.Tests.ViewModels
{
    [TestFixture]
    public class UsersViewModelTests
    {
        [Test]
        public async Task InitializeFetchesTheData()
        {
            //Arrange
            var userService = new Mock<IUserService>();
            var messageService = new Mock<IMessageService>();

            var viewModel = new UsersViewModel(
                userService.Object,
                messageService.Object);

            userService
                .Setup(p => p.GetUsers())
                .Returns(Task.FromResult(Enumerable.Empty<User>()))
                .Verifiable();

            //Act
            await viewModel.Initialize();

            //Assert
            userService.VerifyAll();
        }

        [Test]
        public async Task InitializeAddsFetchedDataToTheCollection()
        {
            //Arrange
            var data = new Fixture().CreateMany<User>().ToList();
            var userService = new Mock<IUserService>();
            var messageService = new Mock<IMessageService>();

            var viewModel = new UsersViewModel(
                userService.Object,
                messageService.Object);

            userService
                .Setup(p => p.GetUsers())
                .ReturnsAsync(data);

            //Act
            await viewModel.Initialize();

            //Assert
            Assert.IsNotEmpty(viewModel.Users);
            Assert.AreEqual(data.Count, viewModel.Users.Count);
            ExtendedAssert.EnumerableEqual(data, viewModel.Users);
        }

        [Test]
        public async Task InitializeShowErrorMessageOnFetchingError()
        {
            //Arrange
            var exceptionMessage = new Fixture().Create<string>();
            var exceptionToThrow = new Exception(exceptionMessage);
            var userService = new Mock<IUserService>();
            var messageService = new Mock<IMessageService>();

            var viewModel = new UsersViewModel(
                userService.Object,
                messageService.Object);

            userService
                .Setup(p => p.GetUsers())
                .Throws(exceptionToThrow);

            //Act
            await viewModel.Initialize();

            //Assert
            messageService.Verify(x => x.ShowError(exceptionMessage));
        }
    }
}
